FROM ubuntu:20.04
MAINTAINER BeastCloud
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update &&  apt-get install -y apache2 libapache2-mod-php php-mysql git php-memcache curl php-cli php-mbstring unzip php-pgsql glusterfs-client wget2 prometheus-apache-exporter supervisor

RUN rm /var/www/html/index.html
ADD bookface/code/* /var/www/html/
ADD beastcloud/init.sh /
ADD beastcloud/config.php /var/www/html

EXPOSE 80

ENTRYPOINT ["/init.sh"]

