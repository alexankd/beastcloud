#!/bin/bash
# Uninstall old versions
sudo apt-get remove docker docker-engine docker.io containerd runc

# Set up the repository

# 1 Update apt package index and install packages to allow apt to use a repository over HTTPS:
sudo apt-get update

sudo apt-get -y install \
  ca-certificates \
  curl \
  gnupg \
  lsb-release
# 2 Add Dockers official GPG key:
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

# 3 Use the following command to set up the stable repository. To add the nightly or test repository, add the word nightly or test (or both) after the word stable in the commands below. Learn about nightly and test channels.
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
# Install Docker engine:

# 1 oppdatere apt package index igjen, og nyeste versjon av Docker Engine
sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io

# 2 To install a specific version of Docker Engine, list the available versions in the repo, then select and install:

# a. List the versions available in your repo:
apt-cache madison docker-ce
# b. Install a specific version using the version string from the second column
sudo apt-get install docker-ce=5:20.10.12~3-0~ubuntu-focal docker-ce-cli=5:20.10.12~3-0~ubuntu-focal containerd.io

# 3 Verify that Docker Engine is installed correctly by running the hello-world image.
sudo docker run hello-world

# sudo modus
sudo su

# Lager mappe
mkdir webServer; cd webServer

# Cloner kyrre sitt repo
git clone https://git.cs.oslomet.no/kyrre.begnum/bookface.git

# Cloner beastCloud sitt repo
git clone https://gitlab.stud.idi.ntnu.no/alexankd/beastcloud.git

# Lager image
docker build -t bftest:v1 -f beastcloud/Dockerfile .

# Kjorer container
docker run --name=docker1 -p 49150:80 -d -P bftest:v1

# Kjører memcache container
docker run --name=memcache -p 11211:11211 -d memcached memcached -m 2048MB
