#!/bin/bash
# sudo modus
sudo su

# Lager mappe
mkdir webServer; cd webServer

# Cloner kyrre sitt repo
git clone https://git.cs.oslomet.no/kyrre.begnum/bookface.git

# Cloner beastCloud sitt repo
git clone https://gitlab.stud.idi.ntnu.no/alexankd/beastcloud.git

# Lager image
docker build -t bftest:v1 -f beastcloud/Dockerfile .

# Kjører container
docker run -d -P bftest:v1