#!/bin/bash

message="$1"
MYIP=$(ip addr show ens3 | grep "inet " | sed -e "s/.* inet //g" | sed -e "s/\/.*//g")
PARENT=$(tr -d "\0" </proc/$PPID/cmdline)

URL="https://discord.com/api/webhooks/948516632286871553/l0XEFj_HJiX5fCGBpnIW9cgS5s77W2AHMUf2uQ_kvBE69lllwLSX_VVQwxh0XKfd7Ozd"

USERNAME="Bookface [$HOSTNAME / $MYIP] $PARENT"

if echo $USERNAME | grep "/var/lib/cloud/"; then
USERNAME="Bookface [$HOSTNAME / $MYIP] cloud-init"
fi

USERNAME=$( echo $USERNAME | sed -e "s/\/bin\/bash//" )

JSON="{\"username\": \"$USERNAME\", \"content\": \"$message\"}"

curl -s -X POST -H "Content-Type: application/json" -d "$JSON" $URL

