#!/bin/bash
source /home/ubuntu/DCSG2003_V22_group44-openrc.sh

if openstack server list | grep Docker | grep SHUTOFF; then
     openstack server start 0f29af4b-9a8a-46b1-9d01-99727dc18327
     discBoot.sh "ChaosMonkey har gått løs på Docker"
     echo Skrur på VM....
     sleep 10
     echo Rebooter VM...?
     openstack server reboot --soft 0f29af4b-9a8a-46b1-9d01-99727dc18327
     discBoot.sh "Docker er tilbake"
     echo Docker er nå oppe
else
     echo Docker er fremdeles oppe
fi


if openstack server list | grep db1 | grep SHUTOFF; then
     openstack server start bb4c2669-2b5f-45b7-bd52-ff0d5069aa98
     discBoot.sh "ChaosMonkey har gått løs på db1"
     echo Skrur på VM....
     sleep 5
     echo Rebooter VM...?
     openstack server reboot --soft bb4c2669-2b5f-45b7-bd52-ff0d5069aa98
     discBoot.sh "db1 er tilbake"
     echo db1 er nå oppe
else
     echo db1 er fremdeles oppe
fi


if openstack server list | grep balancer | grep SHUTOFF; then
     openstack server start 16ae743d-3f3f-47db-b7b0-2f75d5bed705
     discBoot.sh "ChaosMonkey har gått løs på balancer"
     echo balancer er nå oppe
else
     echo balancer er fremdeles oppe
fi

echo Alt er nå oppe hvis det allerede ikke var det
echo "Test" > /home/ubuntu/beastcloud/script/Monitoring/test.txt



# Test on ww1
# openstack server start 7bc48eae-0b7b-46ee-b514-064a49b7a9b7
# openstack server reboot --soft 7bc48eae-0b7b-46ee-b514-064a49b7a9b7
